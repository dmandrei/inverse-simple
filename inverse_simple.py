#!/usr/bin/env python
# coding: utf-8

# In[86]:


import numpy as np
import xarray as xr
import os
import h5py 
import pygmt
import scipy
import h5py
from matplotlib import pyplot as plt

from salvus.mesh.models_1D import model
from salvus.mesh import simple_mesh
import salvus.namespace as sn
from salvus.flow import api

import sys 
sys.path.append(os.path.realpath('./func'))
from functions import *


# In[87]:


def receiver_results(file = "./gravity_neumann/receivers_info.h5"):
    data = h5py.File(file)
    names = np.array(data['names_ACOUSTIC_point']).astype(str)
    values = np.array(data['point']['phi']).reshape(np.array(data['point']['phi']).shape[0])
    return names, values


# In[88]:


mesh = get_mesh(topo = False, moho = False, nex = 24, buffer=5, tensor_order=1, oneD_model = MODEL)
process_some_fields(mesh)

f = np.ones_like(mesh.elemental_fields['RHS'])
mesh.attach_field('M0', f)
mesh.attach_field('M1', M1  * f)

max_radius = max(np.linalg.norm(mesh.points, axis=-1))
mod = model.built_in(MODEL)
r = np.linspace(0.0, max_radius/6371000., 1000)
phi = mod.get_gravitational_potential(r)
g = mod.get_gravity(r, compute_ellipticity_kwargs={})

f = np.ones_like(mesh.elemental_fields['RHS'])
mesh.attach_field('M0', f)
mesh.attach_field('M1', M1  * f)
mesh.attach_field("NEUMANN", -g[-1] * M1 * f)

mesh.find_side_sets_generic("moep", dist, tolerance=290000)

mesh.write_h5("mesh.h5")


# In[89]:


file = open('./receivers/GAGE-Active_forSalvus.txt')
d = np.array(file.readlines())
data1 = np.array([line.strip().split('     ') for line in d][1:])
file.close()

receivers = {str(line[0]): line[1:4].astype(np.float64) for line in data1}

from salvus.flow import simple_config
R = []
s_names = list(receivers.keys())[:5]
for name in s_names:
    R.append(simple_config.receiver.seismology.SideSetPoint3D(latitude=receivers[name][0], 
                                                              longitude=receivers[name][1], 
                                                              depth_in_m=0,#receivers[name][2], 
                                                              side_set_name='r1', 
                                                              station_code=name, 
                                                              fields=['phi']))


# In[90]:


simulation(mesh, R=R, inp='mesh.h5', bc='neumann')


# ### 1st order:

# In[81]:


receiver_results()


# ### 2nd order

# In[85]:


receiver_results()


# ### 4th order:

# In[77]:


receiver_results()


# In[ ]:




