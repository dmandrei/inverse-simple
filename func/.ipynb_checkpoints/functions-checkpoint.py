import numpy as np
import xarray as xr
import os
import h5py 
import pygmt
import scipy
import sys 
import h5py
from matplotlib import pyplot as plt

from salvus.mesh.models_1D import model
from salvus.mesh import simple_mesh
import salvus.namespace as sn
from salvus.flow import api

G = 6.67430e-11
M1 = 1 / (4 * np.pi * G)
R_Earth = 6371.*10**3

MODEL = "prem_iso_one_crust"
output_folder = 'gravity_neumann'
PRECONDITIONER = True



def surface_nodal_indeces(m):
    loc_indeces = np.where(m.elemental_fields['external'][m.side_sets['r1'][0]] == 0)
    gl_indeces = (m.side_sets['r1'][0][loc_indeces], m.side_sets['r1'][1][loc_indeces])
    gl_indeces_full = (np.zeros((gl_indeces[0].shape[0]*25,),dtype=int ), 
                       np.zeros((gl_indeces[0].shape[0]*25,),dtype=int ))
    n = 25
    facets = {'0':list(range(0,25)),
              '1':list(range(100,125)),
              '2':[0, 1, 2, 3, 4,
                   25, 26, 27, 28, 29,
                   50, 51, 52, 53, 54,
                   75, 76, 77, 78, 79,
                   100, 101, 102, 103, 104],
              '3':[20, 21, 22, 23, 24,
                   45, 46, 47, 48, 49,
                   70, 71, 72, 73, 74,
                   95, 96, 97, 98, 99,
                   120, 121, 122, 123, 124],
              '4':list(range(4,125,5)),
              '5':list(range(0,125,5)),}
    for i in range(gl_indeces[0].shape[0]):
        k = i*25
        gl_indeces_full[1][k:k+25] = facets[str(gl_indeces[1][i])]
        gl_indeces_full[0][k:k+25] = [gl_indeces[0][i]]*25
    return gl_indeces_full

def extract_data_from_mesh(m, field='solution'):
    assert field in m.elemental_fields
    z = m.points[m.connectivity][gl_indeces_full][:,2]
    x = m.points[m.connectivity][gl_indeces_full][:,0]
    y = m.points[m.connectivity][gl_indeces_full][:,1]
    data = m.elemental_fields[field][gl_indeces_full][:]
    return x,y,z,data

def get_geographical_coords(x,y,z):
    lat   = np.degrees(np.arctan(z/np.sqrt(y**2 + x**2))) 
    lon   = np.degrees(np.arctan(y/x)) 
    lon[(x < 0.)*(y >= 0.)] = 180 + lon[(x < 0.)*(y >= 0.)]
    lon[(x < 0.)*(y <= 0.)] -= 180. 
    return lon, lat

def interpolate_irregular_grid(lon, lat, data, n=2000):
    lon_res, lat_res = np.meshgrid(np.linspace(-180, 180, n), np.linspace(-90, 90,n))
    a = np.array([lon, lat]).T
    result = scipy.interpolate.griddata(a, values = data, xi=(lon_res.ravel(),lat_res.ravel()), fill_value=np.mean(data))
    return result

def make_dataarray(data, n=2000):
    return xr.DataArray(data = result.reshape(n,n),
                        dims = ['lat', 'lon'],
                        coords = dict(
                            lat = (np.linspace(-90, 90, n)),
                            lon = (np.linspace(-180, 180, n))
                      ))

def pygmt_plot(da, name):
    fig = pygmt.Figure()
    fig.basemap(region='d', projection="R20c", frame=["a",f'+t"{name}"'])
    pygmt.makecpt(cmap="dem2", series=[np.min(result), np.max(result)])
    fig.grdimage(grid=da)
    fig.coast(shorelines="0.5p,black")
    fig.colorbar(frame=["a","y+lX",],)
    fig.show()
    return fig

def get_mesh(topo = False, moho = False, nex = 18, buffer=20, tensor_order= 1, oneD_model = MODEL, ):
    assert type(topo)   == bool
    assert type(moho)   == bool
    assert type(nex)    == int
    assert type(buffer) == int
    assert type(tensor_order) == int
    
    ms = simple_mesh.TidalLoading()
    ms.basic.tidal_loading_file = "./loadmod/tidal_load_m2_10800_new_grad_new_mask.nc"
    ms.basic.tidal_loading_lmax_1 = 256
    ms.basic.tidal_loading_lmax_2 = 256
    ms.basic.nex = nex
    ms.basic.local_refinement_level = 0
    ms.basic.global_refinement_level = 0
    ms.basic.refinement_threshold = 0.05
    ms.basic.model = oneD_model
    
    ms.advanced.tensor_order = tensor_order
    
    ms.gravity_mesh.add_exterior_domain = True
    ms.gravity_mesh.nelem_buffer_surface = 2
    ms.gravity_mesh.nelem_buffer_outer = buffer
    ms.gravity_mesh.dr_basis = 1.5
    
    if topo:
        ms.topography.topography_file="./topography files/topography_earth2014_egm2008_lmax_256.nc"
        ms.topography.topography_varname = 'topography_earth2014_egm2008_lmax_256_lmax_256'
    if moho:
        ms.topography.moho_topography_file= "./topography files/moho_topography_crust_1_0_egm2008.nc"
        ms.topography.moho_topography_varname = 'moho_topography_crust_1_0_egm2008_lmax_256'
        
    return ms.create_mesh(verbose=True)

def process_some_fields(mesh):
    toremove = ['QMU', 'QKAPPA', 'VP', 'VS', 'g', 'GRAD_PHI_X', 'GRAD_PHI_Y', 'GRAD_PHI_Z']
    for field in toremove:
        if field in mesh.elemental_fields.keys():
            del mesh.elemental_fields[field]
    if ('external' in mesh.elemental_fields.keys()) and ('RHO' in mesh.elemental_fields.keys()):
        mask = np.array(mesh.elemental_fields['external'], dtype = bool)
        mesh.elemental_fields['RHO'][mask] = 0.
        mesh.attach_field('RHS', mesh.elemental_fields['RHO'])
        del mesh.elemental_fields['RHO']
        
    mesh.elemental_fields['fluid'] = np.ones(mesh.nelem)
    mesh.map_nodal_fields_to_element_nodal()
    
def simulation(mesh, R=[], inp='./Poisson/mesh.h5', site='local', ranks=10, output_folder='gravity_neumann', bc = 'neumann'):
    assert 'moep' in mesh.side_sets.keys()
    assert 'r2'   in mesh.side_sets.keys()
    assert 'RHS'  in mesh.elemental_fields.keys()
    assert type(R) is list
    
    w = sn.simple_config.simulation.Poisson(mesh=mesh)
    w.domain.polynomial_order = mesh.shape_order
    w.physics.poisson_equation.right_hand_side.filename = inp
    w.physics.poisson_equation.right_hand_side.format = "hdf5"
    w.physics.poisson_equation.right_hand_side.field = "RHS"
    w.physics.poisson_equation.solution.fields = ['gradient-of-phi','solution', 'right-hand-side']
    w.physics.poisson_equation.solution.filename = "grav_sol.h5"
    
  
    
    if bc == 'neumann':
        dirichlet_bc = sn.simple_config.boundary.HomogeneousDirichlet(
           side_sets=["moep"]
        )
        w.add_boundary_conditions(dirichlet_bc)
        neumann_bc = sn.simple_config.boundary.Neumann(
           side_sets=["r2"]
        )
        w.add_boundary_conditions(neumann_bc)
    elif bc == 'dirichlet':
        neumann_bc = sn.simple_config.boundary.HomogeneousDirichlet(
           side_sets=["r2"]
        )
        w.add_boundary_conditions(neumann_bc)


    w.solver.max_iterations = 5000
    w.solver.absolute_tolerance = 0.0
    w.solver.relative_tolerance = 1e-30
    w.solver.preconditioner = True

    # use the last "time" index
    if len(R) > 0:
        w.add_receivers(R)
        w.output.point_data.sampling_interval_in_time_steps = w.solver.max_iterations 
        w.output.point_data.filename = 'receivers.h5'
        w.output.point_data.format = "hdf5"
        
    w.validate()

    api.run(
        input_file=w,
        site_name="local",#
        output_folder= output_folder,
        overwrite=True,
        ranks=ranks,
        verbosity=1,
        get_all = 1,
        wall_time_in_seconds=1000.
    )
    
def dist(x):
    return np.linalg.norm(x, axis=-1)


def read_station_result(s_names, file="./neumann/receivers.h5", field = 'phi'):
    data = h5py.File(file)
    result = []
    if field == 'phi':
        for name in s_names:
            result.append(np.array(data['Waveforms'][f'XX.{name}'][f'XX.{name}..XDA__1970-01-01T00:00:00__1970-01-01T00:00:00__{field}'])[0])
    elif field == 'gradient-of-phi':
        for name in s_names:
            result.append([np.array(data['Waveforms'][f'XX.{name}'][f'XX.{name}..XGN__1970-01-01T00:00:00__1970-01-01T00:00:00__{field}'])[0],
                           np.array(data['Waveforms'][f'XX.{name}'][f'XX.{name}..XGE__1970-01-01T00:00:00__1970-01-01T00:00:00__{field}'])[0],
                           np.array(data['Waveforms'][f'XX.{name}'][f'XX.{name}..XGZ__1970-01-01T00:00:00__1970-01-01T00:00:00__{field}'])[0]])
    return np.array(result)